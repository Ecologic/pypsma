import odin


BASE_API = 'https://api.psma.com.au/beta'

class PSMABuildingSchema(odin.Resource):
    class meta:
        abstract = True
        namespace = "au.com.psma.api"


class Address(PSMABuildingSchema):
    address = odin.StringField(name="addressString", null=True)
    formatted_address = odin.StringField(null=True)
    id = odin.StringField(null=True)


class AddressDetails(PSMABuildingSchema):
    building_ids = odin.TypedArray(odin.StringField(), null=True)


class Building(PSMABuildingSchema):
    address_ids = odin.TypedArray(odin.StringField(), null=True)
    average_eave_height = odin.FloatField(null=True)
    centroid = odin.StringField(null=True)
    elevation = odin.FloatField(null=True)
    footprint_2d = odin.StringField(null=True)
    footprint_3d = odin.StringField(null=True)
    id = odin.StringField(null=True)
    links = odin.StringField(null=True)
    maximum_roof_height = odin.FloatField(null=True)
    roof_complexity = odin.StringField(null=True)
    roof_material = odin.StringField(null=True)
    solar_panel = odin.BooleanField(null=True)
    swimming_pool = odin.BooleanField(null=True)


